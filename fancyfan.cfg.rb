hwmon_base = Dir['/sys/devices/platform/thinkpad_hwmon/hwmon/hwmon*'][0]

@temp_input = File.join hwmon_base, 'temp1_input'
@temp_min = 30
@temp_max = 60

@pwm = File.join hwmon_base, 'pwm1'
@pwn_enable = "#{@pwm}_enable"

@pwm_enable_value = 1
@pwm_disable_value = 0

@pwm_min = 120
@pwm_max = 255

@backdown_interval = 30
@adj_treshold = 2
