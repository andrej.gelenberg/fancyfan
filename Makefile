all: fancyfan

CFLAGS  = -Wall -O2
LDFLAGS = -lrt

CLEAN = *.o fancyfan

-include config.mk

fancyfan: fancyfan.o usage.o
fancyfan.o: Makefile
fancyfan.o: fancyfan.c
usage.o: usage.txt
	ld -r -b binary -o '$@' '$<'

%: %.o
	$(CC) -o '$@' $(CFLAGS) $(filter %.o,$^) ${LDFLAGS}

%.o: %.c
	$(CC) -c -o '$@' $(CFLAGS) '$<'

.PHONY: clean
clean:
	-rm -rf $(CLEAN)
