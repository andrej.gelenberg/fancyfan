#define _GNU_SOURCE

#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

extern const char _binary_usage_txt_start[];
extern const char _binary_usage_txt_end[];

int verbose = 0;

int temp_input = -1;
int pwm_output = -1;
int pwm_enable = -1;

const char *pwm_enable_value  = "1";
const char *pwm_disable_value = "0";

int temp_min = 30000;
int temp_max = 70000;

int pwm_min = 0;
int pwm_max = 255;

int poll_interval = 1000;
int n_aggregate   = 60;

#define msg(...) if ( verbose ) { printf(__VA_ARGS__); }

static int
open_file(
    int *fd,
    int flags,
    const char *path) {

  int ret;

  if ( *fd != -1 ) {
    fprintf(stderr, "other file is already opened\n");
    goto err;
  }

  *fd = open(path, flags);

  if ( *fd == -1 ) {
    fprintf(stderr, "can't open '%s': %m\n", path);
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;

}

static void
usage() {
  fwrite(
      _binary_usage_txt_start,
      _binary_usage_txt_end - _binary_usage_txt_start, 1,
      stderr);
}

int
process_opts(
    int argc,
    char **argv) {

  int ret;
  char *arg;
  int i;
  const char *temp_input_path = NULL;
  const char *pwm_output_path = NULL;
  const char *pwm_enable_path = NULL;

  for(i = 1; i < argc;) {

#define narg (argv[i++])
    arg = narg;

    if ( (arg[0] != '-') || (arg[1] == 0) || (arg[2] != 0) ) {
      fprintf(stderr, "only support short options (%s)\n", argv[i]);
      goto err;
    }

    if ( (i == argc) && strchr("tpwediasfpn", arg[1]) ) {
      fprintf(stderr, "option %s requires an argument\n", arg);
      goto err;
    }

#define num_narg (atoi(narg))

    switch(arg[1]) {
      case 't':
        temp_input_path = narg;
        break;

      case 'p':
        pwm_output_path = narg;
        break;

      case 'w':
        pwm_enable_path = narg;
        break;

      case 'e':
        pwm_enable_value = narg;
        break;

      case 'd':
        pwm_disable_value = narg;
        break;

      case 'i':
        temp_min = num_narg;
        break;

      case 'a':
        temp_max = num_narg;
        break;

      case 's':
        pwm_min = num_narg;
        break;

      case 'f':
        pwm_max = num_narg;
        break;

      case 'l':
        poll_interval = num_narg;
        break;

      case 'n':
        n_aggregate = num_narg;
        break;

      case 'v':
        verbose = 1;
        break;

      default:
        fprintf(stderr, "unsupported option '%s'\n", arg);
        goto err;

    }

#undef narg

  }

  if ( temp_input_path == NULL ) {
    fprintf(stderr, "temp input file is required\n");
    goto err;
  }

  if ( pwm_output_path == NULL ) {
    fprintf(stderr, "pwm output file is required\n");
    goto err;
  }

  if ( open_file(&temp_input, O_RDONLY, temp_input_path) ) goto err;
  if ( open_file(&pwm_output, O_WRONLY, pwm_output_path) ) goto err;


  if ( pwm_enable_path == NULL ) {
    size_t l = strlen(pwm_output_path);
    if ( l > PATH_MAX - sizeof("_enable") ) {
      fprintf(stderr, "path too long\n");
      goto err;
    }
    char *pwm_enable_path = malloc(l + sizeof("_enable"));
    if ( pwm_enable_path == 0 ) {
      fprintf(stderr, "no mem\n");
      goto err;
    }
    memcpy(pwm_enable_path, pwm_output_path, l);
    memcpy(pwm_enable_path + l, "_enable", sizeof("_enable"));

    msg("pwm enable file: %s\n", pwm_enable_path);
    ret = open_file(&pwm_enable, O_WRONLY, pwm_enable_path);
    free(pwm_enable_path);
    if ( ret ) goto err;
  }

  msg(
      "temp min: %d\n"
      "temp max: %d\n"
      "pwm min: %d\n"
      "pwm max: %d\n"
      "pwm enable value: %s\n"
      "pwm disable value: %s\n"
      "polling interval: %d\n"
      "number of aggregations: %d\n",
      temp_min,
      temp_max,
      pwm_min,
      pwm_max,
      pwm_enable_value,
      pwm_disable_value,
      poll_interval,
      n_aggregate);

  ret = 0;
end:
  return ret;

err:
  usage();
  ret = 1;
  goto end;

}

int
init_timer(
    sigset_t *set,
    timer_t *timer) {

  int ret;

  if ( sigemptyset(set) ) {
    fprintf(stderr, "sigemptyset() failed: %m\n");
    goto err;
  }

  if ( sigaddset(set, SIGALRM) ) {
    fprintf(stderr, "sigaddset() failer: %m\n");
    goto err;
  }

  if ( sigprocmask(SIG_BLOCK, set, 0) ) {
    fprintf(stderr, "sigprocmask() failed: %m\n");
    goto err;
  }

  if ( timer_create(CLOCK_MONOTONIC, 0, timer) ) {
    fprintf(stderr, "timer_create() failed: %m\n");
    goto err;
  }

  struct itimerspec ival = {
    {
      poll_interval / 1000,
      ( poll_interval % 1000 ) * 1000
    },
    {
      poll_interval / 1000,
      ( poll_interval % 1000 ) * 1000
    }
  };

  if ( timer_settime(*timer, 0, &ival, 0) ) {
    timer_delete(*timer);
    fprintf(stderr, "timer_settime() failed: %m\n");
    goto err;
  }

  ret = 0;
end:
  return ret;

err:
  ret = 1;
  goto end;


}

int
main_loop() {

  int ret;
  timer_t timer;
  int sig;
  sigset_t set;

  ret = init_timer(&set, &timer);
  if ( ret ) goto err;

  int i;
  for(i = 0 ; i < 3; ++i) {
    printf("bits\n");

    ret = sigwait(&set, &sig);
    if ( ret ) {
      fprintf(stderr, "sigwait failed: %m\n");
      break;
    }
  }

  timer_delete(timer);

err:
  return ret;

}

int
main(
    int argc,
    char **argv) {

  int ret;

  ret = process_opts(argc, argv);
  if ( ret ) goto opts_err;

  ret = main_loop();

opts_err:
  if ( temp_input != -1 ) close(temp_input);
  if ( pwm_output != -1 ) close(pwm_output);
  if ( pwm_enable != -1 ) close(pwm_enable);
  return ret;
}
