#!/usr/bin/ruby

require 'matrix'

if File.file? '/etc/fancyfan.cfg.rb'
  require '/etc/fancyfan.cfg'
elsif File.file? './fancyfan.cfg.rb'
  puts "load local config"
  require './fancyfan.cfg'
else
  throw "can't find config: should be either /etc/fancyfan.cfg.rb or ./fancyfan.cfg.rb"
end

for i in [:@temp_input, :@pwm]
  throw "please define #{i} in the config file" if not defined? i
  throw "#{i} file '#{i}' deosn't exists or is not a regular file" if not File.file? eval i.to_s
end


@temp_min ||= 30
@temp_max ||= 60

@pwm_enable        ||= @pwm + "_enable"
@pwm_enable_value  ||= 1
@pwm_disable_value ||= 0

@pwm_min ||= 0
@pwm_max ||= 255

@backdown_interval ||= 30
@adj_treshold ||= 2

def enable_pwm pwm_enable

  enable = File.open pwm_enable, 'w'

  at_exit do
    enable.write @pwm_disable_value
    enable.close
    puts "reset pwm_enable value to #{@pwm_disable_value}"
  end

  enable.write @pwm_enable_value
  enable.rewind

end

def control_loop temp, pwm

  interval = 1

  cur_pwm  = -1
  cur_time = -1 # tracks back down time
  new_pwm  = -1

  a = (@pwm_max - @pwm_min) / (@temp_max - @temp_min)
  b = @pwm_min - a * @temp_min
  
  loop do
    t = temp.read.to_i / 1000
    temp.rewind

    if t <= 0
      throw "unexpected temperature #{t}, expect values > 0. Probably wrong temp_input device. If that's correct reading feel free to edit this script"
    end

    puts "temp: #{t}"

    if t <= @temp_min
      new_pwm = @pwm_min

    elsif t >= @temp_max
      new_pwm = @pwm_max

    else
      new_pwm = a * t + b

    end

    if new_pwm < cur_pwm

      new_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)

      if (new_time - cur_time) >= @backdown_interval

        cur_time = new_time
        cur_pwm  = new_pwm

        pwm.write cur_pwm
        pwm.rewind

      end

    else

      if new_pwm > cur_pwm

        cur_pwm = new_pwm

        pwm.write cur_pwm
        pwm.rewind

      end

      cur_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    end

    puts "pwm #{cur_pwm} (#{new_pwm})"

    sleep interval

  end

end

def start_fancyfan
  temp = File.open @temp_input, 'r'

  begin

    enable_pwm @pwm_enable

    pwm = File.open @pwm, 'w'

    begin
      control_loop temp, pwm
    ensure
      pwm.close
    end

  ensure
    puts "exit"
    temp.close
  end

end

start_fancyfan
